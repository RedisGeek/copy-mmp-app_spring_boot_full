package com.vikash.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.vikash.modal.OperateurModel;

import antlr.collections.List;

@RepositoryRestResource(collectionResourceRel="exploit" , path="exploit")
public interface RestRepositoies extends PagingAndSortingRepository<OperateurModel, Integer>{
	
}

