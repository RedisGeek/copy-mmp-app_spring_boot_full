package com.vikash.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.vikash.dao.OperateurDao;
import com.vikash.modal.OperateurModel;

@Controller
@RequestMapping("exploit/operateur/")
public class OperateurController {

	@Autowired
	OperateurDao operateurDao;
	
	@Autowired
	public OperateurController() {
		// TODO Auto-generated constructor stub
	}
	
	//Enregistrement
	@PostMapping("/operateur")
	public OperateurModel createOperateur(@Valid @RequestBody OperateurModel om) {
		return operateurDao.save(om);
	}
	
	//affiche operateur
	@GetMapping("/exploitant")
	public List<OperateurModel> getAllOperator(){
		return operateurDao.findAll();
	}
	
	//affiche par ID
	@GetMapping("/operateur/{id}")
	public ResponseEntity<OperateurModel> getById(@PathVariable(value="id") int id){
		
		OperateurModel om=operateurDao.findOne(id);
		
		if (om==null) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok().body(om);
	}
	
	//update by id
	@PutMapping("/operateur/{id}")
	public ResponseEntity<OperateurModel> updateOperateur(@PathVariable(value="id") int id, @Valid @RequestBody OperateurModel omp){
		
		OperateurModel om= operateurDao.findOne(id);
		
		if (om==null) {
			return ResponseEntity.notFound().build();
		}
		
		om.setNOMEXPLOITANT(omp.getNOMEXPLOITANT());
		om.setPRENOMEXPL(omp.getPRENOMEXPL());
		om.setADRESSE(omp.getADRESSE());
		om.setNUMTEL(omp.getNUMTEL());
		om.setNUMCIN(omp.getNUMCIN());
		om.setSEXE(omp.getSEXE());
		om.setDATENAISS(omp.getDATENAISS());
		om.setCOMMUNE(omp.getCOMMUNE());
		
		OperateurModel updateOperateur=operateurDao.save(om);
		return ResponseEntity.ok().body(updateOperateur);
	}
	
	//delete operateur
	@DeleteMapping("/operateur/{id}")
	public ResponseEntity<OperateurModel> deleteOperateur(@PathVariable(value="id") int id){
		
		OperateurModel om=operateurDao.findOne(id);
		if (om==null) {
			return ResponseEntity.notFound().build();
		}
		
		operateurDao.delete(om);
		return ResponseEntity.ok().build();
	}
	
}
