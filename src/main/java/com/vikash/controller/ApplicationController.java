package com.vikash.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.vikash.dao.OperateurDao;
import com.vikash.modal.OperateurModel;
import com.vikash.repository.OperateurRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;



@Controller

public class ApplicationController {

	
	@Autowired
	OperateurDao operateurDao;
	
	@Autowired
	OperateurRepository operateurRepository;
	
	@RequestMapping("/dashboard")
	public String Welcome(HttpServletRequest request) {
		request.setAttribute("mode", "MODE_HOME");
		return "index";
	}

	@RequestMapping("/exploitant")
	public String registration(HttpServletRequest request) {
		
		return "listeExploitant";
	}

	@RequestMapping("/collecteur")
	public String collecteur(HttpServletRequest request) {
		request.setAttribute("mode", "MODE_REGISTER");
		return "listeCollecteur";
	}

	@RequestMapping("/action")
	public String action(HttpServletRequest request) {
		request.setAttribute("mode", "MODE_REGISTER");
		return "listeaction";
	}

	@RequestMapping("/perception")
	public String perception(HttpServletRequest request) {
		request.setAttribute("mode", "MODE_REGISTER");
		return "gestionperception";
	}

	@RequestMapping("/user")
	public String user_operateur(HttpServletRequest request) {
		request.setAttribute("mode", "MODE_REGISTER");
		return "user";
	}


	@RequestMapping("/logout")
	public String logout(HttpServletRequest request) {
		request.setAttribute("mode", "MODE_REGISTER");
		return "deconnection";
	}

}
